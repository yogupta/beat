# beat

### Build step:
Import the project into Intellij idea.
It uses sbt as a build tool.


#### Dependencies

- json - To parse XML to JSON
- spray-json - To output the `case class` objects

#### CLI arguments
1. Absolute path of XML file.
2. The silence duration which reliably indicates a chapter transition.
3. The maximum duration of a segment, after which the chapter will be broken up into multiple segments.
4. A silence duration which can be used to split a long chapter.

Argument 2, 3 and 4 should be ISO 8601 duration strings.
   

  
