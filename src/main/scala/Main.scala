import org.json.{JSONArray, JSONObject, XML}
import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat, enrichAny, JsObject}

import scala.io.Source
import scala.collection.JavaConverters._
import java.time.Duration

import scala.collection.Searching._

case class Silence(from: Duration, to: Duration)
case class Chapter(
    from: Silence,
    to: Silence,
    segments: Array[Silence] = Array.empty
)

case class TitleOffset(title: String, offset: String)
case class Output(segments: Array[TitleOffset])

object OutputJsonProtocol extends DefaultJsonProtocol {
  implicit val titleOffsetFormat: RootJsonFormat[TitleOffset] = jsonFormat2(
    TitleOffset
  )

  implicit object OutputResultFormat extends RootJsonFormat[Output] {
    def read(value: JsValue): Output = Output(
      value.convertTo[Array[TitleOffset]]
    )

    def write(obj: Output): JsObject = JsObject(
      "segments" -> obj.segments.toJson
    )
  }

}

object Main {

  import spray.json._
  import OutputJsonProtocol._

  def main(args: Array[String]): Unit = {
    val xmlPath = args.head
    val silenceIntervalBetweenChapters = Duration.parse(args(1))
    val maxChapterDuration = Duration.parse(args(2))
    val silenceIntervalBetweenSegments = Duration.parse(args(3))

    val allSilences = getAllSilences(xmlPath)
    val chapters: Seq[Chapter] =
      getChapters(
        allSilences,
        maxChapterDuration,
        silenceIntervalBetweenChapters,
        silenceIntervalBetweenSegments
      )
    val output: Seq[TitleOffset] = makeOutput(chapters)
    println(Output(output.toArray).toJson.prettyPrint)
  }

  def makeOutput(chapters: Seq[Chapter]): Seq[TitleOffset] = {
    val output = chapters.zipWithIndex.flatMap { case (chapter, index) =>
      if (chapter.segments.nonEmpty) {
        Array(
          TitleOffset(
            title = s"Chapter ${index + 1}, part 1",
            offset = chapter.from.to.toString
          )
        ) ++
          chapter.segments.zipWithIndex.map { case (segment, segmentIndex) =>
            TitleOffset(
              title = s"Chapter ${index + 1}, part ${segmentIndex + 2}",
              offset = segment.from.toString
            )
          }
      } else {
        Array(
          TitleOffset(
            title = s"Chapter ${index + 1}",
            offset = chapter.from.to.toString
          )
        )
      }
    }
    output
  }

  def getAllSilences(xmlPath: String): Array[Silence] = {
    val xmlJSONObj: JSONArray =
      xmlToJSON(getXMLContent(xmlPath))
        .getJSONObject("silences")
        .getJSONArray("silence")
    val silences: Array[Silence] =
      xmlJSONObj.asScala
        .asInstanceOf[Iterable[JSONObject]]
        .map(parseSilenceEntry)
        .toArray
    silences
  }

  def getChapters(
      silences: Array[Silence],
      maxChapterDuration: Duration,
      silenceIntervalBetweenChapters: Duration,
      silenceIntervalBetweenSegments: Duration
  ): Seq[Chapter] = {
    // get silence intervals whose silence interval is greater than equal to `silenceIntervalBetweenChapters`
    // this will be a single chapter.
    val longSilences =
      Array(Silence(from = Duration.ZERO, to = Duration.ZERO)) ++
        silences.filter(silence => silence.to.minus(silence.from).toNanos >= silenceIntervalBetweenChapters.toNanos) ++
        Array(Silence(from = silences.last.to.plus(maxChapterDuration), to = silences.last.to.plus(maxChapterDuration)))

    val chapters = for {
      i <- 1 until longSilences.length
      from = longSilences(i - 1)
      to = longSilences(i)
      chapter =
        if (to.from.minus(from.to).toNanos > maxChapterDuration.toNanos) {
          // multiple segments between a chapter.
          Chapter(
            from = from,
            to = to,
            segments =
              getSegmentsBetweenTwoIntervals(from = from, to = to, silences = silences, silenceIntervalBetweenSegments)
          )
        } else {
          Chapter(from = from, to = to)
        }
    } yield chapter

    chapters
  }

  /** Get all segment interval between two Silence interval (from, to) Excluding
    * @param from Silence start/from
    * @param to Silence end
    * @param silences all silences in chronological order
    * @param silenceIntervalBetweenSegments silenceIntervalBetweenSegments
    * @return
    */
  def getSegmentsBetweenTwoIntervals(
      from: Silence,
      to: Silence,
      silences: Array[Silence],
      silenceIntervalBetweenSegments: Duration
  ): Array[Silence] = {

    implicit val silenceOrdering: Ordering[Silence] =
      Ordering.by[Silence, (Long, Long)](silence => (silence.from.toNanos, silence.to.toNanos))

    val startIndex = silences.search(from) match {
      case Found(foundIndex)              => foundIndex
      case InsertionPoint(insertionPoint) => insertionPoint
    }

    val endIndex = silences.search(to) match {
      case Found(foundIndex)              => foundIndex
      case InsertionPoint(insertionPoint) => insertionPoint
    }

    val segments = if (endIndex - startIndex == 1) {
      Array.empty[Silence]
    } else {
      // chapter too long and segments exists in a chapter
      silences
        .slice(startIndex, endIndex + 1)
        .filter(silence =>
          silence != from && silence != to && silence.to
            .minus(silence.from)
            .toNanos >= silenceIntervalBetweenSegments.toNanos
        )
    }
    segments
  }

  def getXMLContent(location: String): String = {
    val source = Source.fromFile(location)
    val xml = source.getLines().mkString
    source.close()
    xml
  }

  def xmlToJSON(xml: String): JSONObject = XML.toJSONObject(xml)

  def parseSilenceEntry(silence: JSONObject): Silence =
    Silence(
      from = Duration.parse(silence.getString("from")),
      to = Duration.parse(silence.getString("until"))
    )

}
